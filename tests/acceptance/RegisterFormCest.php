<?php 

class RegisterFormCest{
    const REGISTER_URL = '/register';
    const REGISTER_EMAIL_FIELD_NAME = 'email';
    const REGISTER_PASSWORD_FIELD_NAME = 'password';
    const REGISTER_PASSWORD_RE_FIELD_NAME = 'password_re';
    const REGISTER_SUBMIT_ID = 'field_submit';

    public function _before(AcceptanceTester $I){
        $I->amOnPage(self::REGISTER_URL);
    }

    // tests
    public function emailRequired(AcceptanceTester $I){
        $I->fillField(self::REGISTER_EMAIL_FIELD_NAME, '');
        $I->click(self::REGISTER_SUBMIT_ID);
        $I->see('Email kitöltése kötelező!');
    }

    public function emailNotValid(AcceptanceTester $I){
        $I->fillField(self::REGISTER_EMAIL_FIELD_NAME, 'test');
        $I->click(self::REGISTER_SUBMIT_ID);
        $I->see('Az email nem valid!');
    }

    public function passwordRequired(AcceptanceTester $I){
        $I->fillField(self::REGISTER_PASSWORD_FIELD_NAME, '');
        $I->click(self::REGISTER_SUBMIT_ID);
        $I->see('Jelszó kitöltése kötelező!');
    }

    public function passwordMinLength(AcceptanceTester $I){
        $I->fillField(self::REGISTER_PASSWORD_FIELD_NAME, '111');
        $I->click(self::REGISTER_SUBMIT_ID);
        $I->see('Jelszó mezőnek legalább 6 karakter hosszúnak kell lennie!');
    }

    public function passwordreRequired(AcceptanceTester $I){
        $I->fillField(self::REGISTER_PASSWORD_RE_FIELD_NAME, '');
        $I->click(self::REGISTER_SUBMIT_ID);
        $I->see('Jelszó ismét kitöltése kötelező!');
    }

    public function passwordreMath(AcceptanceTester $I){
        $I->fillField(self::REGISTER_PASSWORD_FIELD_NAME, '123456');
        $I->fillField(self::REGISTER_PASSWORD_RE_FIELD_NAME, '123');
        $I->click(self::REGISTER_SUBMIT_ID);
        $I->see('Jelszó és Jelszó ismét mező értékének eggyeznie kell!');
    }
}
