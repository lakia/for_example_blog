<?php
use \App\Component\Form\Validator\RequiredValidator;
use \App\Component\Form\Validator\EmailValidator;
use \App\Component\Form\Validator\LengthValidator;
use \App\Component\Form\Validator\MatchValidator;
use \App\Component\Form\Element\PasswordElement;
use \App\UnitTestApplication;

class ValidatorsTest extends \Codeception\Test\Unit{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before(){
        if (!defined('ROOT_DIR')) {
            define('DS', DIRECTORY_SEPARATOR);
            define('ROOT_DIR', realpath(__DIR__ . '/../..') . DS);
        }
        require_once ROOT_DIR . 'App/UnitTestApplication.php';
        $unitApp = new UnitTestApplication();
    }

    protected function _after(){

    }

    // tests
    public function testRequired(){
        $validator = new RequiredValidator();
        $validator->setValue('');
        $this->assertFalse($validator->valid());

        $validator->setValue('a');
        $this->assertTrue($validator->valid());
    }

    public function testEmail(){
        $validator = new EmailValidator();
        $validator->setValue('unit_test@test.');
        $this->assertFalse($validator->valid());

        $validator->setValue('unit_test@test.com');
        $this->assertTrue($validator->valid());

        $validator->setValue('');
        $this->assertTrue($validator->valid());
    }

    public function testLength(){
        $validator = new LengthValidator(3, 6);
        $validator->setValue('12');
        $this->assertFalse($validator->valid());

        $validator->setValue('12345678');
        $this->assertFalse($validator->valid());

        $validator->setValue('12345');
        $this->assertTrue($validator->valid());
    }

    public function testMatch(){
        $element = new PasswordElement('xxxx');
        $element->setValue('123');

        $validator = new MatchValidator($element);
        $validator->setValue('12');
        $this->assertFalse($validator->valid());

        $validator->setValue('123');
        $this->assertTrue($validator->valid());

        $element->setValue('');
        $this->assertTrue($validator->valid());
    }
}