# Requirements
php 7.2  
mysql database  
composer  
node js  
php-cli  
# Installation
Clone this repository  
run: "composer install" in root directory  
remove config/*.example files and configure  
create database and configure application in /config/database.php  
create database schema with /src/db/db.sql  
create virtual host to {root} directory  
run: "npm install" in src directory  
run in root folder: "php console.php dependency/updateHandler"  
run in root folder: "php console.php repository/updateHandler"  
## Other
### Add new dependency
Need to create a class:
```
class XyDependency implements DependencyInterface{

    private $dependency;
    
    public function __construct(Config $config, DependencyHandler $diHandler){
        $this->dependency = new Dependency();
    }
    
    public function get() : Dependency{
        return $this->dependency;
    }
}
```
Insert required lines to config(config/console/decendencies.php):
```
'xy' => array(
        'class' => \App\Component\xy\xy::class,
        'dependency' => \App\Component\xy\xyDependency::class,
    ),
```
After this you need to run the following command in console:  
php console.php dependency/updateHandler  
### Add new repository
Insert required lines to config:
```
'xyRepository' => \App\Repository\XyRepository::class,
```
After this you need to run the following command in console:  
php console.php repository/updateHandler
### Regenerate css file
sass --watch src\sass\style.scss:public\assets\css\style.css
### Add new Model and repository
run in root folder for generate files: php console.php model/generate xy --force --timestampModel  
Parameters:  
--force: files will be overwritten if exists  
--timestampModel: Generated modell will be Timestamp model  