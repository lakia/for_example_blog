<?php
use Core\Request\Request;
use App\Controller\ConsoleController;

$routeHandler = $app->getRouteHandler();

$routeHandler->addRoute(
    ConsoleController::class,
    'updateDependenciesHandler')
    ->name('dependency.update')
    ->addQuery('dependency/updateHandler')
    ->addMethod(Request::ALL);

$routeHandler->addRoute(
    ConsoleController::class,
    'updateRepositoriesHandler')
    ->name('repository.update')
    ->addQuery('repository/updateHandler')
    ->addMethod(Request::ALL);

$routeHandler->addRoute(
    ConsoleController::class,
    'generateModel')
    ->name('model.generate')
    ->addQuery('model/generate')
    ->addMethod(Request::ALL);
