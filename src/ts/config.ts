requirejs.config({
    baseUrl: "/",
    paths: {
        jquery: "node_modules/jquery/dist/jquery.min",
        bootstrap: "node_modules/bootstrap/dist/js/bootstrap.bundle.min",
    },
    shim: {
        bootstrap : {
            deps: ["jquery"],
            exports: "bootstrap",
        },
    },
});

requirejs(["jquery", "bootstrap"], function($: JQuery){

});