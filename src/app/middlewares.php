<?php

$middlewareHandler = \App\Application::getInstance()->getMiddlewareHandler();

$middlewareHandler->add(\App\Component\Auth\AuthMiddleware::class);