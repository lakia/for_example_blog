<?php
use Core\Request\Request;
use Core\Route\RouteGroup;
use App\Middleware\TestMiddleware;
use App\Controller\HomeController;
use App\Controller\UserController;

$routeHandler = $app->getRouteHandler();

$routeHandler->addRoute(
    UserController::class,
    'home')
    ->name('home')
    ->addQuery('/')
    ->addMethod(Request::ALL);

$routeHandler->addRoute(
    UserController::class,
    'login')
    ->name('login')
    ->addQuery('/login')
    ->addMethod(Request::ALL);

$routeHandler->addRoute(
    UserController::class,
    'register')
    ->name('register')
    ->addQuery('/register')
    ->addMethod(Request::ALL);


