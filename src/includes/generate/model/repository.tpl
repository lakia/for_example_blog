<?php
namespace App\Repository;

use App\Application;
use App\Component\Data\Exception\WrongModelType;
use App\Component\Data\RDBMS\Model;
use App\Component\Data\RDBMS\Repository;
use App\Model\{modelName};

class {modelName}Repository extends Repository {
    protected $modelName = '{modelName}';
    protected $tableName = '{tableName}';

    public function save({modelName} ${tableName}) : bool {
        return $this->saveMain(${tableName});
    }

    protected function createModelObject(\stdClass $row) : ?{modelName}{
        return $this->createModelObjectMain($row);
    }

    public static function getInstance() : self {
        return Application::getInstance()->getDependencyHandler()->getRepositoryHandler()->get{modelName}Repository();
    }
}