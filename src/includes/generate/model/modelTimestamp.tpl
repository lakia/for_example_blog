<?php
namespace App\Model;

use App\Component\Data\RDBMS\TimestampModel;

class {modelName} extends TimestampModel{
    /** @var array $fields */
    protected $fields = array('id', 'created_at', 'updated_at');

    public function __construct(int $id = null){
        parent::__construct();
    }

    // getters and setters after this line

}