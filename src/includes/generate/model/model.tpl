<?php
namespace App\Model;

use App\Component\Data\RDBMS\Model;

class {modelName} extends Model{
    /** @var array $fields */
    protected $fields = array('id');

    public function __construct(int $id = null){
        parent::__construct();
    }

    // getters and setters after this line

}