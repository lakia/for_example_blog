<?php
return array(
    'element.email.label' => 'Email',
    'element.email.placeholder' => 'Email',
    'element.password.label' => 'Jelszó',
    'element.password.placeholder' => 'Jelszó',
    'element.submit.label' => 'Belépés',
);