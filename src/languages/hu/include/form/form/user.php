<?php
return array(
    'element.email.label' => 'Email',
    'element.email.placeholder' => 'Email',
    'element.password.label' => 'Jelszó',
    'element.password.placeholder' => 'Jelszó',
    'element.password_re.label' => 'Jelszó ismét',
    'element.password_re.placeholder' => 'Jelszó',
    'element.submit.label' => 'Regisztráció',
    'validation.message.email' => array(
        'default' => '{name} nem valid email!',
    ),
    'validation.element.email.email' => array(
        'default' => 'Az email nem valid!',
    ),
);