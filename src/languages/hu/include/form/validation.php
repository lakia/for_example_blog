<?php
return array(
    'messages.required' => array(
        'default' => '{name} kitöltése kötelező!',
    ),
    'messages.email' => array(
        'default' => '{name} nem valid email!',
    ),
    'messages.match' => array(
        'default' => '{elementName} és {name} mező értékének eggyeznie kell!',
    ),
    'messages.length' => array(
        'min' => '{name} mezőnek legalább {min} karakter hosszúnak kell lennie!',
        'max' => '{name} mezőnek maximum {max} karakter hosszúnak kell lennie!',
        'both' => '{name} mezőnek legalább {min} és maximum {max} karakter hosszúnak kell lennie!',
        'default' => '{name} mezőnek legalább {min} és maximum {max} karakter hosszúnak kell lennie!',
    ),
    'messages.exists' => array(
        'default' => '{name} mező értéke már foglalt!',
    ),
    'messages.unique' => array(
        'default' => '{name} már foglalt!',
    ),
);