<?php

return array(
    'headline' => 'Regisztráció',
    'form.formWelcome' => 'Regisztrációhoz töltsd ki a formot!',
    'form.registerSuccessfully' => 'Sikeres regisztráció!',
    'form.registerFailed' => 'Sikeretelen regisztráció!',
    'title' => 'Regisztráció',
);