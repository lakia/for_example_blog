<?php
namespace App\Component\Session;


interface SessionInterface{
    public function get(string $key) : ?string;
    public function set(string $key, string $value) : void;
    public function getArray(string $key) :?array;
    public function setArray(string $key, array $value) : void;
    public function getObject(string $key) : ?object;
    public function setObject(string $key, object $value) : void;
    public function getBool(string $key) : ?bool;
    public function setBool(string $key, bool $value) : void;
    public function unset(string $key) : void;
}