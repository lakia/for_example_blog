<?php
namespace App\Component\Session;


use Core\Component\Config;
use Core\Component\DependencyHandler;
use Core\ComponentInterface\DependencyInterface;

class SessionDependency implements DependencyInterface{

    private $session;

    public function __construct(Config $config, DependencyHandler $diHandler){
        $this->session = new Session();
    }

    public function get(){
        return $this->session;
    }
}