<?php
namespace App\Component\Session;


class Session implements SessionInterface{

    public function __construct(){
        session_start();
    }

    public function get(string $key): ?string{
        return isset($_SESSION[$key]) && is_string($_SESSION[$key]) ? $_SESSION[$key] : null;
    }

    public function set(string $key, string $value): void{
        $_SESSION[$key] = $value;
    }

    public function getArray(string $key): ?array{
        return isset($_SESSION[$key]) && is_array($_SESSION[$key]) ? $_SESSION[$key] : null;
    }

    public function setArray(string $key, array $value): void{
        $_SESSION[$key] = $value;
    }

    public function getObject(string $key): ?object{
        if (isset($_SESSION[$key]) && is_string($_SESSION[$key])) {
            $value = @unserialize($_SESSION[$key]);
            return is_object($value) ? $value : null;
        } else {
            return null;
        }
    }

    public function setObject(string $key, object $value): void{
        $_SESSION[$key] = serialize($value);
    }

    public function getBool(string $key): ?bool{
        if (isset($_SESSION[$key]) && is_string($_SESSION[$key])) {
            $value = @unserialize($_SESSION[$key]);
            return is_bool($value) ? $value : null;
        } else {
            return null;
        }
    }

    public function setBool(string $key, bool $value): void{
        $_SESSION[$key] = serialize($value);
    }

    public function unset(string $key): void{
        unset($_SESSION[$key]);
    }
}