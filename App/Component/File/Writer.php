<?php
namespace App\Component\File;



use App\Component\File\Exception\FileExists;
use App\Component\File\Exception\FilePermission;

class Writer{
    const LINE_BREAK = "\r\n";
    protected $filePath;
    protected $content;
    public function __construct(string $filePath, bool $overwrite = false, bool $root = false){
        $this->filePath = $filePath;
        if (!$root) {
            $this->filePath = ROOT_DIR . $this->filePath;
        }
        if($overwrite && file_exists($this->filePath)){
            throw new FileExists('File already exists.');
        }
        if(!is_writable($this->filePath)){
            throw new FilePermission('File path cannot writeable.');
        }
    }

    public function write(string $string): void{
        $this->content .= $string;
    }

    public function writeLine(string $string) : void{
        $this->content .= $string . "\r\n";
    }

    public function finish(): bool {
        $res = file_put_contents($this->filePath, $this->content);
        return $res === false ? false : true;
    }
}