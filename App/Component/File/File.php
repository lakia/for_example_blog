<?php
namespace App\Component\File;


use App\Component\File\Exception\FileNotExists;
use App\Component\File\Exception\FilePermission;

class File{
    public static function fileExists(string $file) : bool{
        return file_exists(ROOT_DIR . self::getFilenameReplaced($file));
    }

    public static function getFileContent(string $file) : ?string{
        $filepath = ROOT_DIR . self::getFilenameReplaced($file);
        if (file_exists($filepath)) {
            if (is_readable($filepath)) {
                return file_get_contents($filepath);
            } else {
                throw new FilePermission();
            }
        } else {
            throw new FileNotExists();
        }
    }

    public static function getFilenameReplaced(string $file) : string{
        return str_replace('/', DS, $file);
    }
}