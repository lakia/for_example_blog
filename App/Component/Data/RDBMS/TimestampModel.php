<?php
namespace App\Component\Data\RDBMS;


use App\Component\Log\Log;
use Core\Component\Helper;

class TimestampModel extends Model {
    protected $created_at;
    protected $updated_at;
    //protected $fields = array('id', 'created_at', 'updated_at');

    public function __construct(){
        $currentDate = date('Y-m-d H:i:s');
        if ($this->created_at === null) {
            $this->setCreatedAt($currentDate);
        }
        if ($this->updated_at === null) {
            $this->setUpdatedAt($currentDate);
        }
        parent::__construct();
    }

    public function getCreatedAt() : string{
        return $this->created_at;
    }

    public function setCreatedAt(string $dateTime) : void{
        $dateObj = Helper::createDateObject($dateTime);
        if($dateObj != null){
            $this->created_at = $dateObj->format('Y-m-d H:i:s');
        }
    }

    public function getUpdatedAt() : string{
        return $this->updated_at;
    }

    public function setUpdatedAt(string $dateTime) : void{
        $dateObj = Helper::createDateObject($dateTime);
        if($dateObj != null){
            $this->updated_at = $dateObj->format('Y-m-d H:i:s');
        }
    }
}