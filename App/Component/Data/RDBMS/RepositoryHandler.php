<?php
namespace App\Component\Data\RDBMS;


use App\Component\Data\Exception\RepositoryNotFound;
use App\Component\Log\Log;
use Doctrine\DBAL\Driver\Connection;

class RepositoryHandler{
    /** @var RepositoryHandler $instance */
    private static $instance;
    /** @var string $namespace */
    private static $namespace;
    /** @var Connection $db*/
    protected $db;
    public function __construct(Connection $db){
        $this->db = $db;
        self::setInstance($this);
    }

    use Repositories;

    private function setInstance(self $instance) : void{
        self::$instance = $instance;
    }
}