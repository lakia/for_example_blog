<?php
namespace App\Component\Data\RDBMS;


use Core\Component\Config;
use Core\Component\DependencyHandler;
use Core\ComponentInterface\DependencyInterface;

class RepositoryHandlerDependency implements DependencyInterface{
    /** @var RepositoryHandler $repositoryHandler */
    protected $repositoryHandler;

    public function __construct(Config $config, DependencyHandler $diHandler){
        $this->repositoryHandler = new RepositoryHandler($diHandler->getDb());
    }

    public function get(){
        return $this->repositoryHandler;
    }
}