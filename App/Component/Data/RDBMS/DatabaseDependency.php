<?php
namespace App\Component\Data\RDBMS;


use Core\Component\Config;
use Core\Component\DependencyHandler;
use Core\ComponentInterface\DependencyInterface;
use Doctrine\DBAL\FetchMode;

class DatabaseDependency implements DependencyInterface{

    protected $db;

    public function __construct(Config $config, DependencyHandler $diHandler){
        $dbconfig = $config->getAll('database');
        $configuration = new \Doctrine\DBAL\Configuration();
        $connectionParams = array(
            'dbname' => $dbconfig['name'],
            'user' => $dbconfig['user'],
            'password' => $dbconfig['password'],
            'host' => $dbconfig['host'],
            'driver' => 'pdo_mysql',
            'charset' => $dbconfig['charset']
        );
        $this->db = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $configuration);
        $this->db->setFetchMode(FetchMode::STANDARD_OBJECT);
    }

    public function get(){
        return $this->db;
    }
}