<?php
namespace App\Component\Data\RDBMS;


abstract class Model{
    /** @var int $id */
    protected $id;
    /** @var array $fields*/
    protected $fields = array('id');
    public function __construct(){
    }

    public function getId() : ?int{
        return $this->id;
    }
    public function setId(int $id) : void{
        $this->id = $id;
    }

    public function getValues() : array{
        $res = array();
        foreach($this->fields as $field){
            if($this->{$field} != null){
                $res[$field] = $this->{$field};
            }
        }
        return $res;
    }
}