<?php
namespace App\Component\Data\RDBMS;


use Doctrine\DBAL\Connection;

abstract class Repository{
    /** @var string $modelName */
    protected $modelName;
    /** @var string $tableName */
    protected $tableName;
    /** @var Connection $db */
    protected $db;

    public function __construct(Connection $db){
        $this->db = $db;
    }

    public function getById(int $id) : ?Model{
        return $this->getOneByField('id', $id);
    }

    public function delete(Model $model) : bool{
        return false;
    }

    public function deleteById(int $id) : bool{
        return false;
    }

    abstract protected function createModelObject(\stdClass $row);

    protected function saveMain(Model $model) : bool {
        $params = $model->getValues();
        $id = isset($params['id']) ? $params['id'] : $model->getId();
        unset($params['id']);
        if ($id !== null) {
            return $this->db->update($this->tableName, $params, array('id' => $id));
        } else {
            $res = (bool) $this->db->insert($this->tableName, $params);
            if ($res) {
                $model->setId($this->db->lastInsertId());
            }
            return $res;
        }
    }

    public function getOneByField(string $column, string $value) : ?Model{
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder->select('*')->from($this->tableName)->where($column . ' = :' . $column);
        $sql = $queryBuilder->getSQL();
        $stm = $this->db->prepare($sql);
        $stm->bindParam(':' . $column, $value);
        $stm->execute();
        $row = $stm->fetch();
        if (!empty($row)) {
            return $this->createModelObject($row);
        } else{
            return null;
        }
    }

    protected function createModelObjectMain(\stdClass $row) : Model{
        $className = 'App\Model\\' . $this->modelName;
        $model = new $className();
        foreach($row as $field => $value){
            $methodName = 'set';
            $arr = explode('_', $field);
            $n = count($arr);
            for($i = 0; $i < $n; $i++){
                $methodName .= ucfirst($arr[$i]);
            }
            $model->$methodName($value);
        }
        return $model;
    }
}