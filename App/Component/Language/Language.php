<?php
namespace App\Component\Language;

use App\Application;
use App\Component\Log\Log;
use Core\Component\Helper;


class Language implements LanguageInterface{
    protected $filepath;
    protected $lang;
    protected $source;
    public function __construct(string $filepath, string $lang){
        $this->filepath = $filepath;
        $this->lang = $lang;
    }

    public function get(string $name, array $params = array(), string $lang = null) : ?string{
        $lang = $this->getLang($lang);
        $arr = explode('.', $name);
        $filePath = $arr[0];
        if(isset($this->source[$lang][$filePath])){
            unset($arr[0]);
            $key = implode('.', $arr);
            if (isset($this->source[$lang][$filePath][$key])) {
                $source = $this->source[$lang][$filePath][$key];
                if (count($params) > 0) {
                    $searches = array();
                    $replacements = array();
                    foreach ($params as $search => $replace) {
                        $searches[] = '{' . $search . '}';
                        $replacements[] = $replace;
                    }
                    $source = str_replace($searches, $replacements, $source);
                }
                return $source;
            } else {
                Application::getInstance()->getDependencyHandler()->getLog()->warning('Language element is missing: file: ' . $filePath . ', key: ' . $key . ', lang: ' . $lang);
                return null;
            }
        }
        else{
            if($this->loadFile($filePath, $lang)){
                return $this->get($name, $params, $lang);
            } else {
                return null;
            }
        }
    }

    public function getFile(string $filePath, string $lang = null) : ?array{
        $lang = $this->getLang($lang);
        if (isset($this->source[$lang][$filePath])) {
            return $this->source[$lang][$filePath];
        } else {
            if($this->loadFile($filePath, $lang)){
                return $this->getFile($filePath, $lang);
            } else {
                return null;
            }
        }
    }

    private function getLang(string $lang = null) : string{
        return $lang != null ? $lang : $this->lang;
    }

    public function getLanguage() : string{
        return $this->lang;
    }
    public function setLanguage(string $lang) : void{
        $this->lang = $lang;
    }

    protected function loadFile(string $filePath, string $lang){
        $file = $this->filepath . $lang . DS . $filePath . '.php';
        $file = str_replace('/', DS, $file);
        if(file_exists($file)){
            $this->source[$lang][$filePath] = require $file;
            return true;
        } else {
            $this->source[$lang][$filePath] = false;
            Application::getInstance()->getDependencyHandler()->getLog()->warning('Language file is missing in loadFile: ' . $file);
            return false;
        }
    }
}