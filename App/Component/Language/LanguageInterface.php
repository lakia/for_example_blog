<?php
namespace App\Component\Language;

interface LanguageInterface{
    /**
     * return language row value with replaced values
     * @param String $name Route to language source
     * @param array $params Optional Params which needs to be replaced
     * @param String $lang Optional Language text source if null default language will be used
     * @return String return String witch replaced variables or null if not found
     */
    public function get(string $name, array $params = array(), string $lang = null) : ?string;
    /**
     * return language file content
     * @param String $name Route to language source file
     * @param array $params Optional Params which needs to be replaced
     * @param String $lang Optional Language text source if null default language will be used
     * @return String return String witch replaced variables or null if not found
     */
    public function getFile(string $filePath, string $lang = null) : ?array;
    /**
     * @return String get default language
     */
    public function getLanguage() : string;
    /**
     * @param String $language set default language
     */
    public function setLanguage(string $language) : void;
}