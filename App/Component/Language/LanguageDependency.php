<?php
namespace App\Component\Language;


use Core\Component\Config;
use Core\Component\DependencyHandler;
use Core\ComponentInterface\DependencyInterface;

class LanguageDependency implements DependencyInterface{

    private $language;

    public function __construct(Config $config, DependencyHandler $diHandler){
        $this->language = new Language($config->global('lang.resources'), $config->global('lang.default'));
    }

    public function get(){
        return $this->language;
    }
}