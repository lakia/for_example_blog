<?php
namespace App\Component\Auth;

use App\Application;
use App\Component\Log\Log;
use App\Model\User;
use App\Repository\UserRepository;
use Doctrine\DBAL\DBALException;


class Auth implements AuthInterface{

    const SESSION_PREFIX = 'n_auth_';
    const SESSION_USER_ID_KEY = 'user_id';

    /** @var Password $password */
    protected $password;
    /** @var UserRepository $userRepository */
    protected $userRepository;
    /**
     * Logged user Object
     * @var $user User
     */
    protected $user;

    public function __construct(UserRepository $userRepository){
        $this->userRepository = $userRepository;
        $this->password = new Password();
    }

    public function register(User $user): bool {
        $password = $user->getPassword();
        $passwordHash = $this->password->encode($password);
        $user->setPassword($passwordHash);
        try {
            return $this->userRepository->save($user);
        } catch (DBALException $e) {
            $log = Application::getInstance()->getDependencyHandler()->getLog();
            $log->writeLine($e->getMessage(), get_class());
            return false;
        }
    }

    public function login(string $email, string $password): bool {
        $user = $this->userRepository->findByEmail($email);
        if ($user != null) {
            if ($this->password->verify($password, $user->getPassword())) {
                $this->setLoggedUser($user);
                $session = Application::getInstance()->getDependencyHandler()->getSession();
                $session->set(self::SESSION_PREFIX . self::SESSION_USER_ID_KEY, $this->user->getId());
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function can(string $action, User $user = null): bool {
        return false;
    }

    public function hasRole(string $role, User $user = null): bool {
        return false;
    }

    public function logged(): bool {
        return $this->user == null ? false : true;
    }

    public function logout(): void {
        $session = Application::getInstance()->getDependencyHandler()->getSession();
        $session->unset(self::SESSION_PREFIX . self::SESSION_USER_ID_KEY, $this->user->getId());;
    }

    public function getUser() : ?User{
        return $this->user;
    }

    public function setLoggedUser(User $user) : void{
        $this->user = $user;
    }

    public function unsetLoggedUser() : void{
        $this->user = null;
    }
}