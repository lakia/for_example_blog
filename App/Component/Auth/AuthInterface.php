<?php

namespace App\Component\Auth;


use App\Model\User;

interface AuthInterface{

    public function register(User $user): bool;

    public function login(string $email, string $password) : bool;

    public function can(string $action, User $user = null) : bool;

    public function hasRole(string $role, User $user = null) : bool;

    public function logged(): bool;

    public function logout() : void;

    public function getUser() : ?User;

    public function setLoggedUser(User $user) : void;

    public function unsetLoggedUser() : void;

}