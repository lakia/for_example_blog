<?php
namespace App\Component\Auth;


class Password implements PasswordInterface {

    public function encode(string $password) : ?string {
        $hash = password_hash($password, PASSWORD_BCRYPT );
        return $hash != false ? $hash : null;
    }

    public function verify(string $password, string $hash) : bool{
        return password_verify($password, $hash);
    }
}