<?php
namespace App\Component\Auth;


interface PasswordInterface{

    public function encode(string $password) : ?string;

    public function verify(string $password, string $hash) : bool;

}