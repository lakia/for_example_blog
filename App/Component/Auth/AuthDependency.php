<?php
namespace App\Component\Auth;


use App\Application;
use App\Component\Data\RDBMS\RepositoryHandler;
use App\Repository\UserRepository;
use Core\Component\Config;
use Core\Component\DependencyHandler;
use Core\ComponentInterface\DependencyInterface;

class AuthDependency implements DependencyInterface{

    protected $auth;

    public function __construct(Config $config, DependencyHandler $diHandler){
        $this->auth = new Auth(UserRepository::getInstance(), $diHandler->getLog(), Application::getInstance());
    }

    public function get(){
        return $this->auth;
    }
}