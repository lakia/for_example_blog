<?php
namespace App\Component\Auth;


use App\Application;
use App\Component\Data\RDBMS\RepositoryHandler;
use App\Repository\UserRepository;
use Core\ComponentInterface\Middleware;
use Core\Request\Request;
use App\Model\User;

class AuthMiddleware implements Middleware{

    public function before(Request $request) : void{
        // if user is logged in set to dependency
        $app = Application::getInstance();
        $diHandler = $app->getDependencyHandler();
        $session = $diHandler->getSession();
        $userId = $session->get(Auth::SESSION_PREFIX . Auth::SESSION_USER_ID_KEY);
        if ($userId != null) {
            $userRepository = UserRepository::getInstance();
            $user = $userRepository->getById($userId);
            if ($user instanceof User) {
                $auth = Application::getInstance()->getDependencyHandler()->getAuth();
                $auth->setLoggedUser($user);
            }
        }
    }

    public function after(Request $request) : void{

    }
}