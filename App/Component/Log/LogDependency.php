<?php
namespace App\Component\Log;


use Core\Component\Config;
use Core\Component\DependencyHandler;
use Core\ComponentInterface\DependencyInterface;

class LogDependency implements DependencyInterface {
    private $log;
    public function __construct(Config $config, DependencyHandler $diHandler){
        $this->log = new Log($config->global('log.directory'));
    }

    public function get(){
        return $this->log;
    }
}