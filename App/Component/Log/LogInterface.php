<?php
namespace App\Component\Log;


interface LogInterface{
    function writeLine(string $message, string $filename) : void;
    function info(string $message, string $filename) : void;
    function error(string $message, string $filename) : void;
    function warning(string $message, string $filename) : void;
    function debug(string $message, string $filename) : void;
    public static function dumpBackTrace() : void;
    public static function dump() : void;
}