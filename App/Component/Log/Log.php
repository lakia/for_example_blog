<?php
namespace App\Component\Log;


use App\Application;

class Log implements LogInterface {
    protected $filePath = null;
    protected $baseFileName = 'common';
    protected $time;
    protected $requestId;
    public function __construct(string $filepath){
        $this->time = date('Y-m-d H:i:s');
        $this->filePath = $filepath . date('Y-m-d') . DS;
        if (!file_exists($this->filePath)) {
            mkdir($this->filePath, 0777, true);
        }
        $requestIdFilePath = $this->filePath . 'request_id.txt';
        if (file_exists($requestIdFilePath)) {
            $requestId = file_get_contents($requestIdFilePath);
            $this->requestId = ctype_digit($requestId) ? $requestId + 1 : 1;
        } else {
            $this->requestId = 1;
        }
        file_put_contents($requestIdFilePath, $this->requestId);
        $this->writeLine('Request ID: ' . $this->requestId . PHP_EOL . 'Time: ' . $this->time . PHP_EOL, 'request_ids');
    }

    public function writeLine(string $message, string $filename = '') : void{
        $line = $message . PHP_EOL;
        $this->addLine($line, $filename);
    }

    public function info(string $message, string $filename = 'info') : void{
        $line = 'INFO ###########  Message: ' . $message . PHP_EOL;
        $this->addLine($line, $filename);
    }

    public function error(string $message, string $filename = 'error') : void{
        $line = 'ERROR ##########  Message: ' . $message . PHP_EOL;
        $this->addLine($line, $filename);
    }

    public function warning(string $message, string $filename = 'warning') : void{
        $line = 'WARNING ########  Message: ' . $message . PHP_EOL;
        $this->addLine($line, $filename);
    }

    public function debug(string $message, string $filename = 'debug') : void{
        $line = 'DEBUG ##########  Message: ' . $message . PHP_EOL;
        $this->addLine($line, $filename);
    }

    protected function addLine(string $line, string $filename) : void{
        $line = 'Request Id: ' . $this->requestId . ', ' . $line;
        $filePath = $filename != '' ? $this->filePath . str_replace('\\', '_', $filename) . '.txt' :  $this->filePath . $this->baseFileName . '.txt';
        file_put_contents($filePath, $line, FILE_APPEND);
        file_put_contents($this->filePath . 'all.txt', $line, FILE_APPEND);
    }

    public static function dumpBackTrace() : void{
        if (self::canDump()) {
            echo '<br/>===================================================<br/>START<br/>===================================================';
            $backtrace = debug_backtrace();
            echo '<pre>';
            echo 'Backtrace: ';
            var_dump($backtrace[0]);
            echo '</pre>';
            $values = func_get_args();
            echo '<pre>';
            foreach ($values as $value) {
                var_dump($value);
            }
            echo '</pre>';
            echo '===================================================<br/>END<br/>===================================================<br/>';
        }
    }

    public static function dump() : void{
        if (self::canDump()) {
            $values = func_get_args();
            echo '<pre>';
            foreach ($values as $value) {
                var_dump($value);
            }
            echo '</pre>';
        }
    }

    private static function canDump() : bool {
        return Application::getInstance()->getConfig()->globalBool('log.dump.show');
    }
}