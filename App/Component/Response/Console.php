<?php

namespace App\Component\Response;


class Console extends Response{
    protected $message;

    public function __construct(string $message){
        $this->message = $message;
    }

    function getStatus(): int {
        return 200;
    }

    function execute(): void {
        echo $this->message;
    }

    function setStatus(int $status): void {

    }
}