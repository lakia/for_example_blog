<?php
namespace App\Component\Response;

use Core\Response\ResponseInterface;

abstract class Response implements ResponseInterface {
    protected $status = null;

    public function getStatus(): int{
        return $this->status;
    }
}