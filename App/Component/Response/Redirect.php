<?php
namespace App\Component\Response;

class Redirect extends Response{
    protected $url;
    protected $status;

    public function __construct(string $url, int $status = 301){
        $this->url = $url;
        $this->setStatus($status);
    }

    public function setStatus(int $status) : void{
        if(strlen($status) == 3 && substr($status, 0, 1) == 3){
            $this->status = $status;
        }
        else{
            if(!isset($this->status)){
                $this->status = 301;
            }
        }
    }

    function execute() : void{
        header('Location: ' . $this->url, true, $this->status);
    }
}