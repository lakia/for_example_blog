<?php
namespace App\Component\Response;

use App\Application;
use App\Component\Log\Log;
use App\Model\User;

class View extends Response{
    /** @var int $status */
    protected $status;
    /** @var string $viewName */
    protected $viewName;
    /** @var array $params*/
    protected $params;

    public function __construct(string $viewName, array $params = array(), int $status = 200){
        $this->viewName = $viewName . '.html';
        $this->params = $params;
        $this->setStatus($status);
        $this->params['user'] = Application::getInstance()->getDependencyHandler()->getAuth()->getUser();
    }

    public function setStatus(int $status) : void{
        if(strlen($status) == 3){
            $this->status = $status;
        }
        else{
            if(!isset($this->status)){
                $this->status = 200;
            }
        }
    }

    public function execute() : void{
        global $app;
        echo $app->getDependencyHandler()->getView()->render($this->viewName, $this->params);
        http_response_code($this->status);
    }

    public function addValue(string $name, $value) : void{
        $this->params[$name] = $value;
    }

    public function setValues(array $value) : void{
        $this->params = $value;
    }

    public function setValueObject(string $key, object $value) : void{
        $this->params[$key] = $value;
    }

    public function setValue(string $key, string $value) : void{
        $this->params[$key] = $value;
    }
}