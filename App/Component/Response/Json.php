<?php
namespace App\Component\Response;


class Json extends Response{
    protected $status;
    protected $data;


    public function __construct(array $data, $status = 200){
        $this->data = $data;
        $this->setStatus($status);
    }

    public function setStatus(int $status) : void{
        if((int)$status == $status && strlen($status) == 3){
            $this->status = $status;
        }
        else{
            if(!isset($this->status)){
                $this->status = 200;
            }
        }
    }

    public function execute() : void{
        global $app;
        header('Content-Type: application/json');
        http_response_code($this->status);
        echo json_encode($this->data);
    }

    public function setData($data) : void{
        $this->data = $data;
    }

    public function addData($name, $data) : void{
        $this->data[$name] = $data;
    }
}