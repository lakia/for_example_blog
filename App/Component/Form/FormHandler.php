<?php
namespace App\Component\Form;



use App\Component\Form\Exception\ClassNotFoundException;
use App\Component\Form\Exception\WrongInstanceException;
use App\Component\Log\Log;
use Core\Component\DependencyHandler;

class FormHandler{
    /** @var DependencyHandler $dependencyHandler */
    private $dependencyHandler;
    private $forms = array();
    private $validationLang;

    public function __construct(DependencyHandler $dependencyHandler){
        $this->dependencyHandler = $dependencyHandler;
        $this->validationLang = $dependencyHandler->getLang()->getFile('include/form/validation');
    }

    public function create(string $name) : Form{
        $form = new Form($name, $this);
        $form->setLang($this->getFormLang($form));
        $form->init();
        $this->forms[$form->getName()] = $form;
        return $form;
    }

    public function get(string $className) : Form{
        if (!class_exists($className)) {
            throw new ClassNotFoundException('Class not found: ' . $className);
        }
        $arr = explode('\\', $className);
        $name = lcfirst($arr[count($arr) - 1]);
        unset($arr);
        $form = new $className($name, $this);
        if (! $form instanceof Form) {
            throw new WrongInstanceException('Class not App\Component\Form\Form instance: ' . $className);
        }
        $form->setLang($this->getFormLang($form));
        $form->init();
        $this->forms[$form->getName()] = $form;
        return $form;
    }

    public function getDependencyHandler(){
        return $this->dependencyHandler;
    }

    public function getFormLang(Form $form) : array{
        $formLangPath = $form->getFormLangPath();
        return $this->dependencyHandler->getLang()->getFile($formLangPath != null ? $formLangPath : 'include/form/form/' . $form->getName());
    }

    public function getValidationMessage(string $name){
        return isset($this->validationLang['messages.' . $name]) ? $this->validationLang['messages.' . $name] : null;
    }

}