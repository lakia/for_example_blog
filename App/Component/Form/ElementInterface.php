<?php
namespace App\Component\Form;


interface ElementInterface {
    public function getName() : string;

    public function getValidators() : array;

    public function valid() : bool;

    public function addValidator(ValidatorInterface $validator): ElementInterface;

    public function setValue($value);

    public function getValue();

    public function setLabel(string $label) : void;

    public function getLabel() : ?string;
}