<?php
namespace App\Component\Form;


interface FormInterface {
    public function addElement(ElementInterface $element) : ElementInterface;

    public function getElements() : array;

    public function getElement(string $name) : ?ElementInterface;

    public function valid() : bool;

    public function getName() : string;

    public function getValues() : array;

    public function getErrorMessages() : array;

    public function sent() : bool;
}