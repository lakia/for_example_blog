<?php
namespace App\Component\Form\Validator;


use App\ComponentInterface\Form\ValidatorInterface;

class RequiredValidator extends Validator {

    protected $name = 'required';

    public function valid(): bool {
        if (!empty($this->value)) {
            return true;
        } else {
            return false;
        }
    }
}