<?php
namespace App\Component\Form\Validator;


class NotExistsValidator extends Validator{

    protected $name = 'exists';

    protected $table;
    protected $column;

    public function __construct(string $table, string $column){
        $this->table = $table;
        $this->column = $column;
    }

    public function valid(): bool{
        global $app;
        $db = $app->getDependencyHandler()->getDb();
        $rows = $db->select($this->table, array($this->column => $this->value));
        if (count($rows) > 0) {
            return false;
        } else {
            return true;
        }
    }
}