<?php
namespace App\Component\Form\Validator;

use Doctrine\DBAL\DBALException;

/**
 * Check value in database if row found validator will return fail
 * @version 1.0
 */
class UniqueValidator extends Validator {
    /** @var string $name */
    protected $name = 'unique';
    /** @var string $table */
    protected $table;
    /** @var string $column */
    protected $column;

    public function __construct(string $table, string $column){
        $this->table = $table;
        $this->column = $column;
    }
    /**
     * Search for row in the given database table
     * @return bool if row found return false else true
     */
    public function valid(): bool{
        global $app;
        $db = $app->getDependencyHandler()->getDb();
        $queryBuilder = $db->createQueryBuilder();
        $queryBuilder
            ->select('id')
            ->from($this->table)
            ->where('email = :email')
            ->setParameter('email', $this->value);

        $stm = $queryBuilder->execute();
        $res = $stm->fetch();
        return $res === false ? true : false;
    }
}