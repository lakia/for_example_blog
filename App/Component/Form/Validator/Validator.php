<?php
namespace App\Component\Form\Validator;

use App\Component\Form\ValidatorInterface;

abstract class Validator implements ValidatorInterface {

    protected $value;

    protected $name;


    public function getName() : string{
        return $this->name;
    }

    public function setValue($value) : void{
        $this->value = $value;
    }

    public function getErrorMessage(array $message, string $name): string {
        return ucfirst(str_replace('{name}', $name, $message['default']));
    }

}