<?php
namespace App\Component\Form\Validator;


class LengthValidator extends Validator {

    protected $name = 'length';

    protected $min;
    protected $max;

    public function __construct(int $min = null, int $max = null){
        $this->min = $min;
        $this->max = $max;
    }

    public function valid(): bool{
        if (is_array($this->value)) {
            return true;
        } else if (empty($this->value)) {
            return true;
        }
        $length = strlen($this->value);
        if ($this->min != null && $this->min > $length) {
            return false;
        } else if ($this->max != null && $this->max < $length) {
            return false;
        }
        return true;
    }

    public function getErrorMessage(array $message, string $label): string {
        $returnKey = null;
        $searchValue = array('{name}');
        $replaceValue = array($label);
        if ($this->min != null && $this->max != null) {
            $returnKey = 'both';
            $searchValue[] = '{min}';
            $replaceValue[] = $this->min;
            $searchValue[] = '{max}';
            $replaceValue[] = $this->max;
        } else if ($this->min != null) {
            $returnKey = 'min';
            $searchValue[] = '{min}';
            $replaceValue[] = $this->min;
        } else {
            $returnKey = 'max';
            $searchValue[] = '{max}';
            $replaceValue[] = $this->max;
        }
        return str_replace($searchValue, $replaceValue, $message[$returnKey]);
    }
}