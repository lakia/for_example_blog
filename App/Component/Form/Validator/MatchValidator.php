<?php
namespace App\Component\Form\Validator;


use App\Component\Form\Element\Element;

class MatchValidator extends Validator{

    protected $name = 'match';
    /** @var Element $element */
    protected $element;

    public function __construct(Element $element){
        $this->element = $element;
    }

    public function valid(): bool {
        if (!empty($this->element->getValue()) && $this->element->getValue() !== $this->value) {
            return false;
        } else {
            return true;
        }
    }

    public function getErrorMessage(array $message, string $name): string {
        return ucfirst(str_replace(array('{name}', '{elementName}'), array($name, $this->element->getLabel()), $message['default']));
    }
}