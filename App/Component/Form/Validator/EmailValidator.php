<?php
namespace App\Component\Form\Validator;


class EmailValidator extends Validator{

    protected $name = 'email';

    public function valid(): bool{
        if (empty($this->value) || filter_var($this->value, FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            return false;
        }
    }
}