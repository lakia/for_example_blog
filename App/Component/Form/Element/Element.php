<?php
namespace App\Component\Form\Element;

use App\Component\Form\ElementInterface;
use App\Component\Form\Validator\Validator;
use App\Component\Form\ValidatorInterface;

abstract class Element implements ElementInterface {
    protected $name;
    protected $validators = array();
    protected $viewFilePath;
    protected $value;
    protected $failedValidator;
    protected $id;
    protected $attributes = array();
    protected $label;
    public function __construct(string $name){
        $this->name = $name;
        $this->id = 'field_' . $name;
    }

    public function getName(): string {
        return $this->name;
    }

    public function getValidators(): array {
        return $this->validators;
    }

    public function valid(): bool {
        foreach ($this->validators as $validator) {
            $validator->setValue($this->value);
            if(!$validator->valid()){
                $this->failedValidator = $validator;
                return false;
            }
        }
        return true;
    }

    public function setLabel(string $label) : void{
        $this->label = $label;
    }

    public function getLabel() : ?string {
        return $this->label;
    }

    public function addValidator(ValidatorInterface $validator): ElementInterface{
        $this->validators[] = $validator;
        return $this;
    }

    public function setValue($value){
        $this->value = $value;
    }

    public function getValue(){
        return $this->value;
    }

    public function setAttribute(string $name, $value) : void{
        $this->attributes[$name] = $value;
    }

    public function getAttribute(string $name){
        return $this->attributes[$name];
    }

    public function setId(string $id) :void{
        $this->id = $id;
    }

    public function getId() : string{
        return $this->id;
    }

    public function getViewFilePath() : string{
        return $this->viewFilePath;
    }

    public function getFailedValidator() : ?Validator{
        return $this->failedValidator;
    }
}