<?php
namespace App\Component\Form\Element;


use App\Component\Form\Validator\EmailValidator;

class EmailElement extends Element{
    protected $viewFilePath = 'include/form/element/email.html';

    public function __construct(string $name){
        parent::__construct($name);
        $this->addValidator(new EmailValidator());
    }
}