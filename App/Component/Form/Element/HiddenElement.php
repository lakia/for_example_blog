<?php
namespace App\Component\Form\Element;


class HiddenElement extends Element{
    protected $viewFilePath = 'include/form/element/hidden.html';
}