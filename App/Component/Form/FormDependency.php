<?php
namespace App\Component\Form;


use Core\Component\Config;
use Core\Component\DependencyHandler;
use Core\ComponentInterface\DependencyInterface;

class FormDependency implements DependencyInterface {

    private $diHandler;
    private $formHandler;

    public function __construct(Config $config, DependencyHandler $diHandler) {
       $this->diHandler = $diHandler;
       $this->formHandler = new FormHandler($diHandler);
    }

    public function get() {
        return $this->formHandler;
    }
}