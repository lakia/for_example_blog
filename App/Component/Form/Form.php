<?php
namespace App\Component\Form;

use App\Component\Form\Element\Element;
use App\Component\Form\Element\HiddenElement;
use App\Component\Form\Exception\MethodNotImplementedException;
use Core\Component\Helper;
use Core\Request\Request;
use Core\Request\RequestInterface;

class Form implements FormInterface{

    protected $elements = array();
    protected $name;
    /** @var FormHandler $formHandler */
    protected $formHandler;
    protected $formLangPath = null;
    protected $errorMessages = array();
    protected $lang;
    protected $valid = false;
    /** must be URI or URL */
    protected $action = '';
    protected $method = Request::POST;
    protected $validated = false;
    protected $requestParams = array();
    protected $sent = false;

    public function __construct(string $name, FormHandler $formHandler){
        $this->name = $name;
        $this->formHandler = $formHandler;
        $formIdentifier = new HiddenElement('app_form_id');
        $formIdentifier->setValue($this->name);
        $this->addElement($formIdentifier);
        $request = $this->formHandler->getDependencyHandler()->getRequest();
        $method = $request->getMethod();
        $params = null;
        if ($method == RequestInterface::GET) {
            $params = $request->getGetParams();
        } else if ($method == RequestInterface::POST) {
            $params = $request->getPostParams();
        } else {
            throw new MethodNotImplementedException('Method not implemented: ' . $method . ' in form: ' . self::class . ', name: ' . $this->name);
        }
        if (isset($params['app_form_id']) && $params['app_form_id'] == $this->name) {
            $this->requestParams = $params;
            $this->sent = true;
        }
    }

    public function addElement(ElementInterface $element): ElementInterface{
        $this->elements[] = $element;
        $elementName = $element->getName();
        $element->setLabel(isset($this->lang['element.' . $elementName . '.label']) ? $this->lang['element.' . $elementName . '.label'] : $elementName . '_label');
        if (isset($this->requestParams[$elementName])) {
            $element->setValue($this->requestParams[$elementName]);
        }
        return $element;
    }

    public function getElements(): array {
        return $this->elements;
    }

    public function getElement(string $name): ?ElementInterface {
        foreach ($this->elements as $element) {
            if ($element->getName() == $name) {
                return $element;
            }
        }
        return null;
    }

    public function process() : void{
        $this->validate();
    }

    protected function validate() : void{
        if ($this->validated == null) {
            if (!empty($this->requestParams)) {
                $this->validated = true;
                $valid = true;
                foreach ($this->elements as $element) {
                    if (!$element->valid()) {
                        $message = null;
                        $this->errorMessages[$element->getName()] = $this->getErrorMessage($element);
                        $valid = false;
                    }
                }
                $this->valid = $valid;
                if ($this->valid) {
                    $this->finished();
                }
            }
        }
    }

    protected function getErrorMessage(Element $element) : ?string{
        $elementName = $element->getName();
        $validator = $element->getFailedValidator();
        $validatorName = $validator->getName();
        if (isset($this->lang['validation.element' . $elementName . '.' . $validatorName])) {
            $message = $this->lang['validation.element' . $elementName . '.' . $validatorName];
        } else if (isset($this->lang['validation.message' . $validatorName])) {
            $message = $this->lang['validation.message' . $validatorName];
        } else {
            $message = $this->formHandler->getValidationMessage($validatorName);
        }
        return $validator->getErrorMessage($message, $element->getLabel());
    }

    public function getErrorMessages() : array {
        return $this->errorMessages;
    }

    public function getValues() : array{
        $values = array();
        foreach ($this->elements as $element) {
            $values[$element->getName()] = $element->getValue();
        }
        return $values;
    }

    public function finished() : void{}

    public function validated() : bool {
        return $this->validated;
    }

    public function valid(): bool {
        return $this->valid;
    }

    public function init() : void{}

    public function getName() : string{
        return $this->name;
    }

    public function setLang(array $lang) : void{
        $this->lang = $lang;
    }

    public function getLangHandler(Element $element): FormElementLangHandler{
        return new FormElementLangHandler($element, $this->lang);
    }

    public function getText(string $elementName, string $text) : ?string{
        $langKey = 'element.' . $elementName . '.' . $text;
        return isset($this->lang[$langKey]) ? $this->lang[$langKey] : null;
    }

    public function getFormLangPath() : ?string{
        return $this->formLangPath;
    }

    public function setAction(string $action) : void{
        $this->action = $action;
    }

    public function getAction(){
        return $this->action;
    }

    public function setMethod(string $method) : void {
        $method = strtoupper($method);
        if (Request::isValidRequestMethod($method)) {
            $this->method = $method;
        }
    }

    public function getMethod() : string{
        return $this->method;
    }

    public function sent() : bool{
        return $this->sent;
    }
}