<?php
namespace App\Component\Form;


interface ValidatorInterface{

    public function valid() : bool;

    public function setValue($value) : void;

    public function getName() : string;

    public function getErrorMessage(array $message, string $name) : string;

}