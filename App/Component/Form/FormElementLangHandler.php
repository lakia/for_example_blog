<?php
namespace App\Component\Form;


use App\Component\Form\Element\Element;
use App\Component\Log\Log;

class FormElementLangHandler{
    /** @var string $elementName*/
    protected $elementName;
    /** @var array $lang*/
    protected $lang;
    public function __construct(Element $element, array $lang){
        $this->elementName = $element->getName();
        $this->lang = $lang;
    }

    public function get(string $text): ?string{
        $langKey = 'element.' . $this->elementName . '.' . $text;
        return isset($this->lang[$langKey]) ? $this->lang[$langKey] : null;
    }
}