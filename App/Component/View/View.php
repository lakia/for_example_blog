<?php
namespace App\Component\View;


use App\Component\Log\Log;
use Core\Component\Config;
use Core\Component\DependencyHandler;
use Twig\TwigFunction;

class View implements ViewInterface {

    /**
     * @var \Twig\Environment
     */
    private $twig;

    /**
     * @var DependencyHandler;
     */
    private $diHandler;

    public function __construct(Config $config, DependencyHandler $diHandler){
        $loader = new \Twig\Loader\FilesystemLoader($config->global('twig.templates'));
        if ($config->globalBool('twig.cache')) {
            $cache = $config->globalBool('twig.cache.path');
        } else {
            $cache = false;
        }
        $this->twig = new \Twig\Environment($loader, array(
            'cache' => $cache
        ));
        $this->diHandler = $diHandler;
        $this->addTwigFunctions();
    }

    public function render(string $template, array $params): string{
        return $this->twig->render($template, $params);
    }

    private function addTwigFunctions() : void{
        $this->addLangFunctions();
    }

    /** TWIG FUNCTIONS */

    public function addLangFunctions() : void{
        $this->addLangFunction();
    }

    public function addLangFunction() : void{
        $lang = $this->diHandler->getLang();
        $langFunction = new TwigFunction('lang', function(string $value, array $params = array(), string $language = null) use($lang){
            return $lang->get($value, $params, $language);
        });
        $this->twig->addFunction($langFunction);
    }

    /** TWIG FUNCTIONS END */
}