<?php
namespace App\Component\View;


interface ViewInterface{

    public function render(string $template, array $params): string;

}