<?php
namespace App\Component\View;


use Core\Component\Config;
use Core\Component\DependencyHandler;
use Core\ComponentInterface\DependencyInterface;

class ViewDependency implements DependencyInterface{
    /**
     * @var ViewInterface
     */
    private $view;

    public function __construct(Config $config, DependencyHandler $diHandler){

        $this->view = new View($config, $diHandler);
    }

    public function get() : View{
        return $this->view;
    }

}