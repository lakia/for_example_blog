<?php
namespace App\Form;


use App\Component\Form\Form;
use App\Component\Form\Element\EmailElement;
use App\Component\Form\Element\PasswordElement;
use App\Component\Form\Element\SubmitElement;

class LoginForm extends Form {

    protected $formLangPath = 'include/form/form/login';

    public function init() : void{
        $this->addFields();
    }

    protected function addFields() : void {
        $email = new EmailElement('email');
        $this->addElement($email);
        $password = new PasswordElement('password');
        $this->addElement($password);

        $this->addElement(new SubmitElement('submit'));
    }
}