<?php
namespace App\Form;


use App\Component\Form\Element\EmailElement;
use App\Component\Form\Element\PasswordElement;
use App\Component\Form\Element\SubmitElement;
use App\Component\Form\Form;
use App\Component\Form\Validator\LengthValidator;
use App\Component\Form\Validator\MatchValidator;
use App\Component\Form\Validator\RequiredValidator;
use App\Component\Form\Validator\UniqueValidator;


class UserForm extends Form {
    protected $formLangPath = 'include/form/form/user';

    public function init() : void{
        $this->addFields();
    }

    protected function addFields() : void {
        $email = new EmailElement('email');
        $email->addValidator(new RequiredValidator())->addValidator(new UniqueValidator('user', 'email'));
        $this->addElement($email);
        $password = new PasswordElement('password');
        $password->addValidator(new RequiredValidator())->addValidator(new LengthValidator(6));
        $this->addElement($password);
        $passwordRe = new PasswordElement('password_re');
        $passwordRe->addValidator(new RequiredValidator())->addValidator(new MatchValidator($password));
        $this->addElement($passwordRe);

        $this->addElement(new SubmitElement('submit'));
    }
}