<?php
namespace App\Model;

use App\Component\Data\RDBMS\TimestampModel;

class Post extends TimestampModel{
    /** @var string $title */
    protected $title;
    /** @var string $content*/
    protected $content;
    /** @var array $fields */
    protected $fields = array('id', 'title', 'content', 'created_at', 'updated_at');

    public function __construct(int $id = null){
        $this->id = $id;
        parent::__construct();
    }

    // getters and setters after this line

    public function getTitle() : ?string{
        return $this->title;
    }

    public function setTitle(string $title) : void {
        $this->title = $title;
    }

    public function getContent() : ?string{
        return $this->content;
    }

    public function setContent(string $content) : void{
        $this->content = $content;
    }

}