<?php
namespace App\Model;

use App\Component\Data\RDBMS\TimestampModel;

class User extends TimestampModel{
    /** @var string $email */
    protected $email;
    /** @var string $password */
    protected $password;
    /** @var array $fields */
    protected $fields = array('id', 'email', 'password', 'created_at', 'updated_at');

    public function __construct(string $email = null, string $password = null){
        $this->email = $email;
        $this->password = $password;
        parent::__construct();
    }

    // getters and setters after this line

    public function getEmail() : ?string{
        return $this->email;
    }

    public function setEmail(string $email): void {
        $this->email = $email;
    }

    public function getPassword() : ?string{
        return $this->password;
    }

    public function setPassword(string $password): void {
        $this->password = $password;
    }
}