<?php
namespace App\Repository;

use App\Application;
use App\Component\Data\Exception\WrongModelType;
use App\Component\Data\RDBMS\Model;
use App\Component\Data\RDBMS\Repository;
use App\Model\User;

class UserRepository extends Repository {
    protected $modelName = 'User';
    protected $tableName = 'user';

    public function findByEmail(string $email) : ?User{
        return $this->getOneByField('email', $email);
    }

    public function save(User $user) : bool {
        return $this->saveMain($user);
    }

    protected function createModelObject(\stdClass $row) : ?User{
        return $this->createModelObjectMain($row);
    }

    public static function getInstance() : self {
        return Application::getInstance()->getDependencyHandler()->getRepositoryHandler()->getUserRepository();
    }
}