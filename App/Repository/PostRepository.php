<?php
namespace App\Repository;

use App\Application;
use App\Component\Data\Exception\WrongModelType;
use App\Component\Data\RDBMS\Model;
use App\Component\Data\RDBMS\Repository;
use App\Model\Post;

class PostRepository extends Repository {
    protected $modelName = 'Post';
    protected $tableName = 'post';

    public function save(Post $post) : bool {
        return $this->saveMain($post);
    }

    protected function createModelObject(\stdClass $row) : ?Post{
        return $this->createModelObjectMain($row);
    }

    public static function getInstance() : self {
        return Application::getInstance()->getDependencyHandler()->getRepositoryHandler()->getPostRepository();
    }
}