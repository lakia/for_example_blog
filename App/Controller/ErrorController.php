<?php
namespace App\Controller;


use App\Component\Response\View;
use Core\ComponentInterface\Controller;
use Core\Response\ResponseInterface as Response;

class ErrorController extends Controller{
    public function error404() : Response{
        return new View("404", array(), 404);
    }
}