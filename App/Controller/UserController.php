<?php
namespace App\Controller;


use App\Component\Data\RDBMS\Repository;
use App\Component\Data\RDBMS\RepositoryHandler;
use App\Component\Log\Log;
use App\Component\Response\Console;
use App\Component\Response\Redirect;
use App\Component\Response\View;
use App\Model\Post;
use Core\ComponentInterface\Controller;
use Core\Response\ResponseInterface as Response;
use App\Form\LoginForm;
use App\Form\UserForm;
use App\Model\User;

class UserController extends Controller {

    public function home() : Response{
        $auth = $this->app->getDependencyHandler()->getAuth();
        if ($auth->logged()) {
            return new View('user/user/home');
        } else {
            return $this->login();
        }
    }

    public function login() : Response{
        $loginForm = $this->dependencyHandler->getFormHandler()->get(LoginForm::class);
        $loginForm->process();
        $values = $loginForm->getValues();
        $auth = $this->app->getDependencyHandler()->getAuth();
        if (isset($values['email']) && isset($values['password']) && $auth->login($values['email'], $values['password'])) {
            return new Redirect('/');
        } else {
            $view = new View('public/user/login');
            $view->addValue('form', $loginForm);
            if ($loginForm->sent()) {
                $view->addValue('showErrorMessage', true);
            }
            return $view;
        }
    }

    public function register() : Response{
        $view = new View('public/user/register');
        $registerForm = $this->dependencyHandler->getFormHandler()->get(UserForm::class);
        $registerForm->process();
        $registerRoute = $this->app->getRouteHandler()->getRoute('register');
        if ($registerRoute != null) {
            $registerForm->setAction($registerRoute->getUri());
        }
        if ($registerForm->valid()) {
            $values = $registerForm->getValues();
            $user = new User();
            $user->setEmail($values['email']);
            $user->setPassword($values['password']);
            $view->addValue('registerSuccess', $this->dependencyHandler->getAuth()->register($user));
        }
        $this->dependencyHandler->getDb();
        $view->addValue('form', $registerForm);
        return $view;
    }
}