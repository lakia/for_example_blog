<?php
namespace App\Controller;


use App\Application;
use App\Component\File\File;
use App\Component\File\Writer;
use App\Component\Log\Log;
use App\Component\Response\Console;
use Core\ComponentInterface\Controller;
use Core\Request\Request;
use Core\Response\ResponseInterface as Response;

class ConsoleController extends Controller{

    public function generateModel(Request $request) : Response{
        try{
            $params = $request->getGetParams();
            if (empty($params[2])) {
                $this->app->getDependencyHandler()->getLog()->warning('Model generate error argument 3 is missing!');
                return new Console('Model generate error argument 3 is missing!');
            } else {
                $modelName = ucfirst($params[2]);
            }
            $timestampModel = false;
            $force = false;
            $n = count($params);
            for ($i = 3; $i < $n; $i++) {
                $value = $params[$i];
                if ($value == '--timestampModel') {
                    $timestampModel = true;
                } else if ($value == '--force') {
                    $force = true;
                }
            }
            return $this->generateModelHelper($modelName, $timestampModel, $force);
        }
        catch (\Exception $e){
            return new Console($e->getMessage());
        }
    }

    public function updateDependenciesHandler() : Response{
        try{
            $config = $this->app->getConfig();
            $classNamespace = $config->get('app/dependency.handler.namespace');
            $className = $config->get('app/dependency.handler.class');
            $classPath = $classNamespace . '\\' . $className;
            $filePath = str_replace('\\', DS, $classPath) . '.php';
            $dependencies = $config->getAll('dependencies');
            $fileWriter = new Writer($filePath);
            $fileWriter->write($this->getDependencyHandlerTraitContent($classNamespace, $className, $dependencies));
            if ($fileWriter->finish()) {
                return new Console('Dependencies handler trait updated, output file: ' . $filePath);
            } else {
                return new Console('Something went wrong write to file save was don\'t successfull');
            }
        }
        catch (\Exception $e){
            return new Console($e->getMessage());
        }
    }

    public function updateRepositoriesHandler() : Response{
        try{
            $config = $this->app->getConfig();
            $classNamespace = $config->get('app/repository.handler.namespace');
            $className = $config->get('app/repository.handler.class');
            $classPath = $classNamespace . '\\' . $className;
            $filePath = str_replace('\\', DS, $classPath) . '.php';
            $repositories = $config->getAll('repositories');
            $fileWriter = new Writer($filePath);
            $fileWriter->write($this->getRepositoryHandlerTraitContent($classNamespace, $className, $repositories));
            if ($fileWriter->finish()) {
                return new Console('Repository handler trait updated, output file: ' . $filePath);
            } else {
                return new Console('Something went wrong write to file save was don\'t successfull');
            }
        }
        catch (\Exception $e){
            return new Console($e->getMessage());
        }
    }

    public function pathNotFound() : Response{
        return new Console('Requested path not found');
    }

    private function generateModelHelper(string $modelName, bool $timestampModel, bool $force) : Response{
        $config = $this->app->getConfig();
        $modelFilePath = $config->get('app/model.generate.model.namespace') . '\\' . $modelName . '.php';
        $repositoryFilePath = $config->get('app/model.generate.repository.namespace') . '\\' . $modelName . 'Repository.php';
        $continue = true;
        if (!$force) {
            if (File::fileExists($modelFilePath)) {
                $continue = false;
            } else if (File::fileExists($repositoryFilePath)) {
                $continue = false;
            }
            if (!$continue) {
                return new Console('Model or Repository already exists use --force to overwrite');
            }
        }
        $this->generateModelFile($modelName, $modelFilePath, $timestampModel);
        $this->generateRepositoryFile($modelName, $repositoryFilePath);
        return new Console('Model and Repository generate is successfull!');
    }

    private function generateModelFile(string $modelName, string $modelFilePath, bool $timestampModel) : void{
        $config = $this->app->getConfig();
        if ($timestampModel) {
            $modelContent = File::getFileContent($config->get('app/model.generate.template.model.timestamp'));
        } else {
            $modelContent = File::getFileContent($config->get('app/model.generate.template.model'));
        }
        $modelContent = str_replace('{modelName}', $modelName, $modelContent);
        $modelWriter = new Writer($modelFilePath);
        $modelWriter->write($modelContent);
        $modelWriter->finish();
    }

    private function generateRepositoryFile(string $modelName, string $repositoryFilePath) : void{
        $config = $this->app->getConfig();
        $repositoryContent = File::getFileContent($config->get('app/model.generate.template.repository'));
        $repositoryContent = str_replace(['{modelName}', '{tableName}'], [$modelName, lcfirst($modelName)], $repositoryContent);
        $repositoryWriter = new Writer($repositoryFilePath);
        $repositoryWriter->write($repositoryContent);
        $repositoryWriter->finish();
    }

    private function getRepositoryHandlerTraitContent(string $classNamespace, string $className, array $repositories) : string{
        $content = '<?php' . Writer::LINE_BREAK . '/* Generated file please don\'t overwrite it manually, file is generated by: ConsoleController::updateRepositoriesHandler in console mode */' . Writer::LINE_BREAK . 'namespace ' . $classNamespace . ';' . Writer::LINE_BREAK . Writer::LINE_BREAK .
            'trait ' . $className . '{' . Writer::LINE_BREAK;
        foreach ($repositories as $alias => $class) {
            $class = '\\' . $class;
            $getterName = 'get' . ucfirst($alias);
            $content .= "\t" . 'protected $' . $alias . ';' . Writer::LINE_BREAK . Writer::LINE_BREAK .
                "\t" . 'public function ' . $getterName . '() : ' . $class . '{' . Writer::LINE_BREAK .
                "\t\t" . 'if ($this->' . $alias . ' == null) { ' . Writer::LINE_BREAK .
                "\t\t\t" . '$this->' . $alias . ' = new ' . $class . '($this->db);' . Writer::LINE_BREAK .
                "\t\t" . '}' . Writer::LINE_BREAK .
                "\t\t" . 'return $this->' . $alias . ';' . Writer::LINE_BREAK .
                "\t" . '}' . Writer::LINE_BREAK . Writer::LINE_BREAK;
        }
        $content .= '}';
        return $content;
    }

    private function getDependencyHandlerTraitContent(string $classNamespace, string $className, array $dependencies) : string{
        $content = '<?php' . Writer::LINE_BREAK . '/* Generated file please don\'t overwrite it manually, file is generated by: ConsoleController::updateDependenciesHandler in console mode */' . Writer::LINE_BREAK . 'namespace ' . $classNamespace . ';' . Writer::LINE_BREAK . Writer::LINE_BREAK .
            'trait ' . $className . '{' . Writer::LINE_BREAK;
        foreach ($dependencies as $alias => $data) {
            $class = '\\' . $data['class'];
            $diClass = '\\' . $data['dependency'];
            $getterName = 'get' . ucfirst($alias);
            $content .= "\t" . 'protected $' . $alias . ';' . Writer::LINE_BREAK . Writer::LINE_BREAK .
                "\t" . 'public function ' . $getterName . '() : ' . $class . '{' . Writer::LINE_BREAK .
                "\t\t" . 'if ($this->' . $alias . ' == null) { ' . Writer::LINE_BREAK .
                "\t\t\t" . '$di = new ' . $diClass . '($this->config, $this);' . Writer::LINE_BREAK .
                "\t\t\t" . '$this->' . $alias . ' = $di->get();' . Writer::LINE_BREAK .
                "\t\t" . '}' . Writer::LINE_BREAK .
                "\t\t" . 'return $this->' . $alias . ';' . Writer::LINE_BREAK .
                "\t" . '}' . Writer::LINE_BREAK . Writer::LINE_BREAK;
        }
        $content .= '}';
        return $content;
    }
}