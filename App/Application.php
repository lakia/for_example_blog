<?php
namespace App;
use App\Component\Log\Log;
use Core\ApplicationException;
use Core\Component\DependencyHandler;
use Core\Component\MiddlewareHandler;
use Core\Component\Config;
use Core\Request\Request;
use Core\Response\ResponseInterface;
use Core\Route\Route;
use Core\Route\RouteHandler;

class Application{
    /** @var Config $config*/
    protected $config;
    /** @var Request $request */
    protected $request;
    /** @var RouteHandler $routeHandler */
    protected $routeHandler;
    /** @var MiddlewareHandler $middlewareHandler */
    protected $middlewareHandler;
    /** @var DependencyHandler $dependencyHandler */
    protected $dependencyHandler;
    /** @var string $uri */
    protected $method;
    /** @var string $uri */
    protected $uri;
    /** @var Application $instance*/
    private static $instance;
    public function __construct(string $configPath, string $requestUri, string $requestMethod){
        $uri = explode('?', $requestUri);
        $this->uri = $uri[0];
        $this->method = $requestMethod;
        self::setInstance($this);
    }

    public function load(string $configPath, string $globalfilePath) : void {
        require_once ROOT_DIR . 'Core' . DS . 'Component' . DS . 'Config.php';
        $this->config = new Config($configPath, $globalfilePath);
        spl_autoload_register(function ($className) {
            $autoloadPaths = $this->config->globalArray('autoload.mapping');
            $array = explode('\\', $className);
            $namespaceRoot = $array[0];
            if(isset($autoloadPaths[$namespaceRoot])){
                unset($array[0]);
                $filePath = $autoloadPaths[$namespaceRoot];
                foreach ($array as $value){
                    $filePath .= $value . DS;
                }
                $filePath = substr($filePath, 0, -1);
                $filePath .= '.php';
                if(file_exists($filePath)){
                    require_once $filePath;
                }
            }
        });
    }

    public function prepare(array $getParams, array $postParams) : void{
        $this->request = new Request($getParams, $postParams);
        $this->request->setMethod($this->method);
        $this->request->setURI($this->uri);
        $this->dependencyHandler = new DependencyHandler($this);
        $this->middlewareHandler = new MiddlewareHandler($this);
        $this->routeHandler = new RouteHandler($this);
        if ($this->config->getBool('app/middleware.include')) {
            require_once $this->config->get('app/middleware.file.path');
        }
    }

    public function run() : void{
        $customControllerStatus = $this->request->getAttribute('customControllerStatus');
        $customControllerStatusValue = $this->config->get('app/controller.custom.' . $customControllerStatus);
        if ($customControllerStatusValue != null) {
            $arr = explode('.', $customControllerStatusValue);
            $route = $this->routeHandler->createRoute($this->request->getUri(), $this->request->getMethod(), $arr[0], $arr[1]);
        } else {
            $route = $this->routeHandler->getRouteByRequest($this->request);
            if($route == null){
                /** 404 error handling */
                $arr = explode('.', $this->config->get('app/controller.custom.404'));
                $route = $this->routeHandler->createRoute($this->request->getUri(), $this->request->getMethod(), $arr[0], $arr[1]);
                $this->getDependencyHandler()->getLog()->writeLine('404 route: uri: ' . $this->request->getUri() . ', method: ' . $this->request->getMethod(), '404');
            }
            $this->getDependencyHandler()->getLog()->writeLine('route: uri: ' . $this->request->getUri() . ', method: ' . $this->request->getMethod(), 'routes');
        }
        $this->middlewareHandler->prepare();
        $this->middlewareHandler->runBefore($this->request);
        if ($this->request->getAttribute('customController')) {
            $customControllerRow = $this->config->get('app/controller.custom.' . $this->request->getAttribute('customController'));
            $customController = explode('.', $customControllerRow);
            $controllerClass = $customController[0];
            $controllerMethod = $customController[1];
            $this->executeCustomRoute($route, $controllerClass, $controllerMethod);
        } else {
            $this->executeRoute($route);
        }
        $this->middlewareHandler->runAfter($this->request);
    }

    protected function executeCustomRoute(Route $route, string $controllerClass, string $controllerMethod) : void{
        $controller = new $controllerClass($this);
        $response = $controller->{$controllerMethod}($this->request);
        if(!$response instanceof ResponseInterface){
            throw new ApplicationException('Controller return value must be App\Component\Core\Response\Response implementation: ' . $controllerClass . '::' . $controllerMethod);
        }
        $response->execute();
    }

    protected function executeRoute(Route $route) : void{
        $controllerName = $route->getControllerName();
        $controller = new $controllerName($this);
        $response = $controller->{$route->getControllerMethodName()}($this->request);
        if(!$response instanceof ResponseInterface){
            throw new ApplicationException('Controller return value must be App\Component\Core\Response\Response implementation: ' . $controllerName . '::' . $route->getControllerMethodName());
        }
        $response->execute();
    }

    public function getRequest() : Request{
        return $this->request;
    }

    public function getConfig() : Config{
        return $this->config;
    }

    public function getDependencyHandler() : DependencyHandler{
        return $this->dependencyHandler;
    }

    public  function getRouteHandler() : RouteHandler{
        return $this->routeHandler;
    }

    public function getMiddlewareHandler() : MiddlewareHandler{
        return $this->middlewareHandler;
    }

    private static function setInstance(Application $instance) : void{
        self::$instance = $instance;
    }

    public static function getInstance() : Application{
        return self::$instance;
    }
}