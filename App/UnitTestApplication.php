<?php
namespace App;

use Core\Component\Config;


class UnitTestApplication{
    protected $config;
    public function __construct(){
        require_once ROOT_DIR . 'App' . DS . 'Core' . DS . 'Component' . DS . 'Config.php';
        $this->config = new Config(ROOT_DIR . 'config' . DS . 'test' . DS);
        spl_autoload_register(function ($className) {
            global $app;
            $autoloadPaths = $this->config->get('base.autoloadMapping');
            $array = explode('\\', $className);
            $namespaceRoot = $array[0];
            if (isset($autoloadPaths[$namespaceRoot])) {
                unset($array[0]);
                $filePath = $autoloadPaths[$namespaceRoot] . DS;
                foreach ($array as $value) {
                    $filePath .= $value . DS;
                }
                $filePath = substr($filePath, 0, -1);
                $filePath .= '.php';
                if(file_exists($filePath)){
                    require_once $filePath;
                }
            }
        });
    }
}