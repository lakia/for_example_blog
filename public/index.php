<?php
use Core\Component\Helper;
use App\Application;

define('START_TIME', explode(' ', microtime()));
define('ROOT_DIR',  dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR);
define('DS', DIRECTORY_SEPARATOR);

require_once ROOT_DIR . 'vendor' . DS . 'autoload.php';

require_once ROOT_DIR . 'App' . DS . 'Application.php';
$app = new Application('app', $_SERVER['REQUEST_URI'], $_SERVER['REQUEST_METHOD']);
$app->load(ROOT_DIR . 'config' . DS. 'app' . DS, ROOT_DIR . 'config' . DS . 'global.php');
$app->prepare($_GET, $_POST);
require Helper::getReplacedFilePath($app->getConfig()->get('app/routes.filepath'));
$app->getRouteHandler()->finalize();
$app->run();