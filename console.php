<?php
use Core\Component\Helper;
use App\Application;

if(empty($argv[1])){
    throw new Exception('Argument 1(path) is required');
}

define('START_TIME', explode(' ', microtime()));
define('DS', DIRECTORY_SEPARATOR);
define('ROOT_DIR', dirname(__FILE__) . DS);

require_once ROOT_DIR . 'vendor' . DS . 'autoload.php';

require_once ROOT_DIR . 'App' . DS . 'Application.php';

$app = new Application('console', $argv[1], 'GET');
require_once ROOT_DIR . 'vendor' . DS . 'autoload.php';
$app->load(ROOT_DIR . 'config' . DS. 'console' . DS, ROOT_DIR . 'config' . DS . 'global.php');
$app->prepare($argv, array());
$config = $app->getConfig();
require Helper::getReplacedFilePath($config->get('app/routes.filepath'));
$app->getRouteHandler()->finalize();
$app->run();