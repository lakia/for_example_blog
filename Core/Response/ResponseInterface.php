<?php
namespace Core\Response;


interface ResponseInterface{

    function getStatus(): int;

    function execute() : void;

    function setStatus(int $status) : void;
}