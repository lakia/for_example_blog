<?php
namespace Core\ComponentInterface;


use App\Application;
use Core\Component\DependencyHandler;
use Core\Request\Request;

abstract class Controller{

    /** @var Application $app */
    protected $app;
    /** @var DependencyHandler $dependencyHandler */
    protected $dependencyHandler;
    public function __construct(Application $app){
        $this->app = $app;
        $this->dependencyHandler = $app->getDependencyHandler();
    }
}