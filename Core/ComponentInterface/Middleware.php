<?php
namespace Core\ComponentInterface;


use Core\Request\Request;

interface Middleware{

    public function before(Request $request);

    public function after(Request $request);
}