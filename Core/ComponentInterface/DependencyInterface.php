<?php

namespace Core\ComponentInterface;


use Core\Component\Config;
use Core\Component\DependencyHandler;

interface DependencyInterface{
    public function __construct(Config $config, DependencyHandler $diHandler);
    public function get();
}