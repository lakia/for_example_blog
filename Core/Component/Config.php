<?php
namespace Core\Component;

use App\Component\Log\Log;

class Config{
    /** @var array $config */
    protected $config = array();
    /** @var string $configPath */
    protected $configPath;
    /** @var array $global */
    protected $global;

    public function __construct(string $configPath, string $globalFilePath){
        $this->configPath = str_replace('/' , DIRECTORY_SEPARATOR, $configPath);
        $this->global = require $globalFilePath;
    }

    public function global(string $name) : ?string{
        return isset($this->global[$name]) && is_string($this->global[$name]) ? $this->global[$name] : null;
    }

    public function globalBool(string $name) : ?bool{
        return isset($this->global[$name]) && is_bool($this->global[$name]) ? $this->global[$name] : null;
    }

    public function globalArray(string $name) : ?array{
        return isset($this->global[$name]) && is_array($this->global[$name]) ? $this->global[$name] : null;
    }

    public function get(string $name) : ?string{
        $array = explode('/', $name);
        if(isset($this->config[$array[0]])){
            return isset($this->config[$array[0]][$array[1]]) && is_string($this->config[$array[0]][$array[1]]) ? $this->config[$array[0]][$array[1]] : null;
        }
        else{
            $this->loadConfig($array[0]);
            return $this->get($name);
        }
    }

    public function getBool(string $name) : ?bool {
        $array = explode('/', $name);
        if(isset($this->config[$array[0]])){
            return isset($this->config[$array[0]][$array[1]]) && is_bool($this->config[$array[0]][$array[1]]) ? $this->config[$array[0]][$array[1]] : null;
        }
        else{
            $this->loadConfig($array[0]);
            return $this->getBool($name);
        }
    }

    public function getArray(string $name) : ?array{
        $array = explode('/', $name);
        if(isset($this->config[$array[0]])){
            return isset($this->config[$array[0]][$array[1]]) && is_array($this->config[$array[0]][$array[1]]) ? $this->config[$array[0]][$array[1]] : null;
        }
        else{
            $this->loadConfig($array[0]);
            return $this->getArray($name);
        }
    }

    public function getAll(string $name) : ?array {
        if(isset($this->config[$name])){
            return isset($this->config[$name]) ? $this->config[$name] : null;
        }
        else{
            $this->loadConfig($name);
            return $this->getAll($name);
        }
    }

    protected function loadConfig(string $filename) : void{
        $filepath = $this->configPath . str_replace('/' , DIRECTORY_SEPARATOR, $filename) . '.php';
        if(file_exists($filepath)){
            $this->config[$filename] = require $filepath;
        } else {
            throw new \Exception("Config file missing: " . $filepath);
        }
    }
}