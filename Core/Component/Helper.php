<?php
namespace Core\Component;

use Core\Request\RequestInterface;
use DateTime;

class Helper{

    public static function convertToArray($element) : array {
        if(is_array($element)){
            return $element;
        }
        else{
            return array($element);
        }
    }

    public static function getRuntime() : float{
        $endTime = explode(' ', microtime());
        return ($endTime[0] - START_TIME[0]) + ($endTime[1] - START_TIME[1]);
    }

    public static function replaceVariables(string $string, array $values = array()) : string{
        if(empty($values)){
            return $string;
        }
        else{
            $keys = array();
            foreach ($values as $key => $val){
                $keys[] = '{' . $key . '}';
            }
            return str_replace($keys, array_values($values), $string);
        }
    }

    public static function createDateObject(string $dateString) : ?\DateTime{
        $date = DateTime::createFromFormat('Y-m-d G:i:s', $dateString);
        if ($date !== false) {
            return $date;
        }
        else{
            $time = strtotime($dateString);
            if($time != -1){
                return new \DateTime($time);
            }
            else{
                return null;
            }
        }
    }

    public static function getReplacedFilePath(string $filePath): string {
        return str_replace('/', DIRECTORY_SEPARATOR, $filePath);
    }
}