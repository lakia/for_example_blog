<?php
namespace Core\Component;

use App\Application;
use Core\ComponentInterface\Middleware;
use Core\Request\Request;

class MiddlewareHandler{
    protected $middlewares = array();
    protected $middlewareObject = array();
    protected $config;
    protected $app;

    public function __construct(Application $app){
        $this->app = $app;
        $this->config = $this->app->getConfig();
    }

    public function add(string $className, array $params = null) : void{
        $this->middlewares[] = array(
            'class' => $className,
            'params' => $params,
        );
    }

    public function prepare() : void{
        foreach($this->middlewares as $middleware){
            $className = $middleware['class'];
            if(!class_exists($className)){
                throw new \Exception('Middleware Class not found: ' . $className);
            }
            $obj = new $className($this->app, $middleware['params']);
            if(!$obj instanceof Middleware){
                throw new \Exception('Middleware class not implements Core\ComponentInterface\Middleware interface: ' . $className);
            }
            $this->middlewareObject[] = $obj;
        }
    }

    public function runBefore(Request $request) : void{
        foreach ($this->middlewareObject as $middleware){
            $middleware->before($request);
        }
    }

    public function runAfter(Request $request) : void{
        for($i = (count($this->middlewares) - 1); $i >= 0; $i--){
            $this->middlewareObject[$i]->after($request);
        }
    }
}