<?php
namespace Core\Component;

use App\Application;
use App\Component\Dependencies;
use Core\Request\Request;

class DependencyHandler{

    protected $dependencies;
    /** @var Config $config */
    protected $config;
    /** @var Application $app*/
    protected $app;
    public function __construct(Application $app){
        $this->app = $app;
        $this->config = $this->app->getConfig();
    }

    public function getRequest(): Request{
        return $this->app->getRequest();
    }

    use Dependencies;
}