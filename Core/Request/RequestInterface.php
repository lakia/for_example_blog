<?php
namespace Core\Request;

interface RequestInterface{
    const GET = 'GET';
    const POST = 'POST';
    const PUT = 'PUT';
    const DELETE = 'DELETE';
    const PATCH = 'PATCH';
    const HEAD = 'HEAD';
    const OPTIONS = 'OPTIONS';
    const ALL = 'ALL';
    /**
     * @return array return merged array of post and get params
     */
    function getParams() : array;
    /**
     * return param if found else return null
     * @param String $name param name
     * @return mixed return found param or null if not found
     **/
    function getParam(string $name) : string;
    /**
     * @return array Array of get params
     */
    function getGetParams() : array;
    /**
     * @return array Array of post params
     */
    function getPostParams() : array;
    /**
     * @param String $method
     */
    function setMethod(string $method) : void;

    function getMethod() : string;
    /**
     * @param String $uri
     */
    function setUri(string $uri) : void;

    function setAttribute(string $name, $value) : void;

    function getAttribute($name);

    public function setGetParams(array $pathParams) : void;

    public function setPostParams(array $postParams) : void;
};