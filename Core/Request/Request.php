<?php
namespace Core\Request;

use Core\Component\Helper;

class Request implements RequestInterface {
    protected $getParams = array();
    protected $postParams = array();
    protected $method;
    protected $uri;
    protected $attributes = array();

    public function __construct($getParams, $postParams){
        $this->getParams = $getParams;
        $this->postParams = $postParams;
    }

    public function getParams() : array{
        return array_merge($this->pathParams, $this->postParams);
    }

    public function getParam($name) : string{
        if(isset($this->postParams[$name])){
            return $this->postParams[$name];
        }
        else if($this->pathParams[$name]){
            return $this->pathParams[$name];
        }
        else{
            return null;
        }
    }

    public function getGetParams() : array{
        return $this->getParams;
    }

    public function setGetParams(array $getParams) : void{
        $this->getParams = $getParams;
    }

    public function getPostParams() : array{
        return $this->postParams;
    }

    public function setPostParams(array $postParams) : void{
        $this->postParams = $postParams;
    }

    public function getMethod() : string{
        return $this->method;
    }

    public function setMethod(string $method) : void{
        $method = strtoupper($method);
        if (self::isValidRequestMethod($method)) {
            $this->method = $method;
        }
    }

    public function getUri() : string{
        return $this->uri;
    }

    public function setUri(string $uri) : void{
        $this->uri = $uri;
    }

    public function setAttribute(string $name, $value) : void{
        $this->attributes[$name] = $value;
    }

    public function getAttribute($name) : ?string{
        return isset($this->attributes[$name]) ? $this->attributes[$name] : null;
    }

    public static function isValidRequestMethod(string $method) : bool {
        if ($method == RequestInterface::GET ||
            $method == RequestInterface::POST ||
            $method == RequestInterface::PUT ||
            $method == RequestInterface::DELETE ||
            $method == RequestInterface::PATCH ||
            $method == RequestInterface::HEAD ||
            $method == RequestInterface::OPTIONS) {
            return true;
        } else {
            return false;
        }
    }
}