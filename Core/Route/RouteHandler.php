<?php
namespace Core\Route;

use App\Application;
use Core\Request\Request;

class RouteHandler{
    protected $routes = array();
    protected $routesIndexes = array();
    protected $routeIndex = 0;
    protected $app;
    protected $config;
    protected $middlewareHandler;

    public function __construct(Application $app){
        $this->app = $app;
        $this->config = $this->app->getConfig();
        $this->middlewareHandler = $this->app->getMiddlewareHandler();
    }

    public function getRouteByRequest(Request $request) : ?Route{
        $requestMethod = $request->getMethod();
        $uri = $request->getUri();
        foreach($this->routes as $route){
            $queries = $route->getQueries();
            foreach($queries as $query){
                $query = str_replace('/', '\/', $query);
                $query = '/^' . preg_replace('/{([a-z0-9_]+)}/', '[0-9a-zA-Z]+', $query) . '$/';
                if(preg_match($query, $uri)){
                    $methods = $route->getMethods();
                    foreach ($methods as $method){
                        if($method == Request::ALL || $method == $requestMethod){
                            $middlewares = $route->getMiddlewares();
                            if(count($middlewares) > 0){
                                foreach ($middlewares as $middleware) {
                                    $this->middlewareHandler->add($middleware);
                                }
                            }
                            return $this->createRoute($query, $method, $route->getControllerName(), $route->getControllerMethod());
                        }
                    }
                }
            }
        }
        return null;
    }
    public function addRoute(string $controllerClass, string $controllerMethod) : RouteCommand{
        $command = new RouteCommand($controllerClass, $controllerMethod);
        $this->addRouteCommand($command);
        return $command;
    }

    public function createRoute(string $query, string $method, string $controllerName, string $controllerMethod) : Route{
        return new Route($query, $method, $controllerName, $controllerMethod);
    }

    public function getRoute($name) : ?Route{
        if(isset($this->routes[$this->routesIndexes[$name]])){
            $route = $this->routes[$this->routesIndexes[$name]];
            return $this->createRoute($route->getQueries()[0], $route->getMethods()[0], $route->getControllerName(), $route->getControllerMethod());
        }
        return null;
    }

    public function finalize() : void{
        foreach($this->routes as $route){
            $name = $route->getName();
            if($name != null){
                $this->routesIndexes[$name] = $route->getId();
            }
        }
    }

    public function addGroup($query, Callable $groupFunction) : void{
        $group = new RouteGroup($query);
        $groupFunction($group);
        $group->finalize($this);
    }

    public function addRouteCommand(RouteCommand $command) : RouteCommand{
        $this->routes[] = $command;
        $command->setId($this->routeIndex);
        $this->routeIndex++;
        return $command;
    }
}