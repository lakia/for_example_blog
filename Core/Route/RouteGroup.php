<?php
namespace Core\Route;


class RouteGroup{
    protected $routes;
    protected $baseQuery;
    public function __construct($baseQuery){
        $this->baseQuery = $baseQuery;
    }

    public function addRoute($query, $method, string $controllerClass, string $controllerMethod) : RouteCommand{
        $command = new RouteCommand($this->baseQuery . $query, $method, $controllerClass, $controllerMethod);
        $this->routes[] = $command;
        return $command;
    }

    public function finalize(RouteHandler $routeHandler) : void{
        foreach($this->routes as $route){
            $routeHandler->addRouteCommand($route);
        }
    }
}