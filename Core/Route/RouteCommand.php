<?php
namespace Core\Route;


use Core\Component\Helper;

class RouteCommand{
    protected $queries = array();
    protected $methods = array();
    protected $controllerName;
    protected $controllerMethod;
    protected $name = null;
    protected $id = null;
    protected $middlewares = array();
    public function __construct(string $controllerClass, string $controllerMethod){
        $this->controllerName = '\\' . $controllerClass;
        $this->controllerMethod = $controllerMethod;
    }

    public function getControllerName() : string{
        return $this->controllerName;
    }

    public function getControllerMethod(): string{
        return $this->controllerMethod;
    }

    public function name($name) : self{
        $this->name = $name;
        return $this;
    }
    public function setId($id) : void{
        $this->id = $id;
    }
    public function getId() : int{
        return $this->id;
    }
    public function getName() : string{
        return $this->name;
    }

    public function getQueries() : array{
        return $this->queries;
    }

    public function getMethods() : array{
        return $this->methods;
    }

    public function add($class) : void{
        $this->middlewares[] = '\\' . $class;
    }

    public function getMiddlewares() : array{
        return $this->middlewares;
    }

    public function addQuery(string $query) : self{
        $this->queries[] = $query;
        return $this;
    }

    public function addMethod(string $method) : self{
        $this->methods[] = $method;
        return $this;
    }
}