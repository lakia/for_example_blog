<?php
namespace Core\Route;

use Core\Component\Helper;

class Route{
    protected $query;
    protected $method;
    protected $controllerClass;
    protected $controllerMethod;
    protected $args = null;

    public function __construct(string $query, string $method, string $controllerName, string $controllerMethod){
        $this->query = $query;
        $this->method = $method;
        $this->controllerClass = $controllerName;
        if(!class_exists($this->controllerClass)){
            throw new \Exception('Controller class not found: ' . $this->controllerClass);
        }
        $this->controllerMethod = $controllerMethod;
        if(!method_exists($this->controllerClass, $this->controllerMethod)){
            throw new \Exception('Controller method not found: ' . $this->controllerClass . '::' . $this->controllerMethod);
        }
    }

    public function getControllerName() : string{
        return $this->controllerClass;
    }

    public function getControllerMethodName() : string{
        return $this->controllerMethod;
    }

    public function getUri(array $values = array()) : string{
        return Helper::replaceVariables($this->query, $values);
    }
}